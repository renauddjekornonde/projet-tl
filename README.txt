						*********************  AUTOMATE APP   ********************


AUTOMATE APP est une application conçu en deux langages de programmation différent, nottament python et java pour vous permettre de parcourir et de manipuler vos automates.

Pour utiliser les applications :
	* Python
		Vous devez vous rendre dans le dossier automatePython et ouvrir le fichier automate.py puis exécuter ce fichier avec votre outil de 					développement ou sur votre terminal avec la commande python automate.py

		Prérequis: Pour que toutes les fonctionnalités de l'application fonctionnent normalement vous devez d'abors installer la bibliothèque prettyTable
			Pour installer prettyTable vous devez télécharger la dernière version de python puis l'installer et en suite la commande pip install 			prettytable sur votre terminal.
	* Java
		L'application en java à été développé sous Eclipse. Telecharger et installer l'IDE Eclipse puis ouvrez sous Eclipse le dossier automateJava que vous 			venez de télécharger. Sur votre explorateur sous Eclipse, ouvrez successivement les deux dossiers automatejava qui apparaissent avec un double clic 			ou en cliquant sur la pétite flêche. Vous verrez à l'interieur de ces dossiers un ficher automate.java. Double cliquez sur le ficher puis exécuter 			avec le button run.

		Prérequis: Pour que toutes les fonctinnalités de l'application marchent normalement vous devez télécharger le fichier .jar de la bibliothèque JTable 		via web puis ajouter le ficher .jar a la classpath du projet. Description : Propriété/java Build path/librairies/classpath/Add External jARs

Les fonctionnalités disponibles de l'application:
	1. Savoir si son automate est déterministe ou non déterministe;
	2. Convertir son automate non déterministes en déterministe;
	3. Visualiser globalement son automate sur un tableau.
	
	Dès que vous exécutez les fichiers de l'application vous dévriez très facilement vous repérez et choisir le type d'opération que vous voulez faire.

L'application AUTOMATE APP à été conçu par DJEKORNONDE RAOUDEL Renaud, Etudiant en troisième année de de Bachelor Génie Logiciel. En cas de besoin ou de problème vous pouvez me contacter sur ce mail: renaud.djekornonde@supdeco.edu.sn
		