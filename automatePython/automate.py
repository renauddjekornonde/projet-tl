# Extension allowing to draw table
from prettytable import PrettyTable

#Creation of the PLC object
class Automate:
    states = []
    alphabet = []
    transitions = []
    initialState = []
    finalState= []

    #constructor which takes as input the name of
    # the file containing the automaton
    def __init__(self, nameFile):

        # Opening the file automate.text and reading
        # each line of the file's contents
        with open(nameFile, 'r') as f:
            lines = f.readlines()

        # Variable allowing to affect each part of the automaton


        for line in lines:

        # In each line we check the beginning of the sentence
        # then we remove the spaces, the useless elements and we
        # separate the useful elements by commas

            if line.startswith('states'):
                self.states = line.strip().split(':')[1].split(',')
            elif line.startswith('alphabet'):
                self.alphabet = line.strip().split(':')[1].split(',')
            elif line.startswith('Eo'):
                self.initialState = line.strip().split(':')[1].split(',')

            elif line.startswith('F'):
                self.finalState = line.strip().split(':')[1].split(',')

            elif line.startswith('transition:'):
                continue
            transition_strs = line.strip().split(',')
            if len(transition_strs)>=3 and (line.startswith('0') or line.startswith('1')
                                            or line.startswith('2') or line.startswith('3')or
                                            line.startswith('4')or line.startswith('5')):
                begin_state= transition_strs[0]
                symbol= transition_strs[1]
                end_state = transition_strs[2]

                self.transitions.append({'begin': begin_state,
                                         'symbol': symbol, 'end': end_state})

            # When it encounters the transition sentence: in a line,
            # it abandons this line and continues reading with
            # the following lines.
            # For each line following this sentence, it
            # removes spaces and separates
            # the elements of this line by commas.
            # Each line must contain three elements,
            # it assigns the first in begin_state
            # the second in symbol and the last in end_state
            # then adds the whole in the array transitions

    def createTable(automate):
        # Creation of the table containing the transitions
        table = PrettyTable()
        table.field_names = [''] + list(automate.alphabet)

        #field_name adds a name to the table header then
        # adds a list of alphabets next to the header

        for state in automate.states:
            row = [state]
            for symbol in automate.alphabet:
                transition = None
                for element in automate.transitions:
                    if element['begin'] == state and element['symbol'] == symbol:
                        transition = element
                        break

                if transition:
                    row.append(transition['end'])
                else:
                    row.append('')
            table.add_row(row)
        return table

            # It goes through each element of states,
            # alphabet and checks if it is equal
            # to each line that we had previously added in the transitions
            # table through the begin, symbol and end attributes
            #If they are set, it adds the transition end in the row of the table

    def get_initialState(self):
        return self.initialState
    #Method allowing to recover the initial states

    def get_finalState(self):
        return self.finalState
    #Method for recovering the final states

    def get_transitions(self, state, symbol):
        listingTransition= []
        for element in self.transitions:
            if element['begin']==state and element['symbol']==symbol:
                listingTransition.append(element)
        return listingTransition
    #Method allowing to recover the transitions of the automaton

    def displayTable(table):
        print(table)

    def printStates(automate):
        finalResult = str(automate.get_initialState())
        finalResult = finalResult.replace('[', '{').replace(']', '}')
        print("Eo= ",finalResult)

        finalResult = str(automate.get_finalState())
        finalResult = finalResult.replace('[', '{').replace(']', '}')
        print("F= ",finalResult)

        # final Result retrieve the array of initialState and/or
        # finalState transforms them into a character string then replaces
        # the square brackets with braces to facilitate display


    # Function that checks whether the given automaton is
    # deterministic or non-deterministic

    def is_deterministic(automaton):
        # We go through all the states of the automaton
        for state in automaton.states:
        # We check if each state has at most one transition for each symbol
            for symbol in automaton.alphabet:
                if len(automaton.get_transitions(state, symbol)) > 1:
                    return False
    # If no state has more than one transition for a symbol,
    # then the automaton is deterministic
        return True



    #Method that allows you to ask the user what he wants
    # after entering his file containing the automaton
    def operationChoice(test):
        if Automate.is_deterministic(test):
            print("You have entered a Deterministic Automaton")
        else:
            print("You have entered a Non Deterministic Automaton",
                  "\nPress 1 to convert to a Deterministic Automaton",
                  "\nPress 2 to cancel this operation")

            #this block manages exceptions, when
            # user enters a value than a number, it
            # will generate an error message
            try:
                operation= int(input("Answer: "))
                if operation==1 :
                    print("This operation is currently unavailable")
                while operation==2 :
                    break
            except ValueError:
                print("Invalid input. Please Please enter a number")


    #Method to search for a word in automaton
    def wordSearch(self,word):
        # Initialization of all the states (current state and initial state)
        # of the automaton
        for state in self.get_initialState():
            currentStates = set([state])
            elementInWorld= set()

            # For each symbol of the word
            for symbole in word:

                # Initialization of the variable that will contain the next state
                nextStates = set()

                # For each current state
                for currentState in currentStates:
                    # For each transition starting from this current state
                    for transition in self.transitions:
                        #If the input vertex is in the current state and the symbol
                        # read in this transition is equal to the first symbol
                        # contained in the word that the user typed
                        if transition['begin'] == currentState and transition['symbol'] == symbole:

                            # Addition of the arrival state of this transition to the set of following states
                            nextStates.add(transition['end'])
                            #Displays this symbol is in this transition
                            print(symbole," is in the transition (",transition['begin'],","
                                  ,transition['end'],")\n")
                        #If not add the symbol in the symbol in the table of words
                        elif transition['begin'] != currentState and transition['symbol'] == symbole:
                            elementInWorld.add(symbole)

                    # Update of all current states
                    currentStates = nextStates

            # Checking if one of the current states is final
            for state in currentStates:
                if state in self.finalState:
                    print(word," is recognized by the automaton")
                    return True

            # If none of the current states is final,
            # the word is not recognized by the automaton
            for elements in elementInWorld:
                print(elements," is not in any transition\n")
            print(word," is not recognized by the automaton")
            return False

    def welcom(self):
        #The set of operations to choose when opening the application
        print("Enter 1 to verify the determination of your Automaton\n"
        	, "Enter 2 to convert your automaton\r\n"
        	, "Enter 3 to test your Automaton with words\r\n"
        	, "Enter 4 to display your automaton.\r\n"
        	, "Enter 0 to exit.");

print("Welcome aboard our app.\r\n"
        , "Please choose an operation.\r")
choice= 1
while choice== 1:
    #creation of an instance with as argument the path where our text file is.
    test= Automate('automatepython/automate.txt')
     #greeting message display
    test.welcom()

    try:
        #Variable that will retrieve the user's choice
        operation= int(input("Answer: "))
        if operation==1 :
            Automate.operationChoice(test)
        elif operation==2 :
            print("This operation is currently unavailable")
        elif operation==3 :
            #opening of the keyboard to retrieve the user's word
            # and pass the word to the Word search function
            word= ""
            word= input("Enter your word ")
            test.wordSearch(word)

        elif operation==4 :
            testTable=Automate.createTable(test)
            Automate.displayTable(testTable)
            Automate.printStates(test)
        elif operation==0:
            break
        else:
            print("You entered a wrong number\n")
        #Call the operationChoise method when the user's choice is equal to 1
        #Call the createTable method and then
        # printStates when the user's choice is equal to 4
        print("1. Continue\n0. Exit")
        choice= int(input("Answer: "))
    except ValueError:
        print("Invalid input. Please enter a number \n")