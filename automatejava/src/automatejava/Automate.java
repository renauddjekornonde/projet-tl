package automatejava;
//BufferedReader is a class in Java that
//allows to read text files line by line
import java.io.BufferedReader;

//FileReader is a class in Java that allows
//to read text files character by character
import java.io.FileReader;

//Java library interface that defines a
//set of common operations for lists
import java.util.List;

//Java library interface that provides a data
//structure for storing key-value pairs
import java.util.Map;

//class of the Java library which makes it easier to read
import java.util.Scanner;

//class from the Java library that allows you
//to create dynamic arrays
import java.util.Vector;

//an implementation of the List interface
import java.util.ArrayList;

//class from the Java library that implements the Map interface
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;

//class from Java's swing library that allows to create a window with
//basic options such as size, visibility, position, close
import javax.swing.JFrame;

//Class of the Java Swing graphics library that allows
//you to add a scroll bar to a component
import javax.swing.JScrollPane;

//Class of Java's Swing graphics library that
//allows you to create and display tables of data
import javax.swing.JTable;

import java.util.Set;

//Creation of the PLC object
public class Automate {
	protected Vector<String> state = new Vector<String>();
    protected Vector<String> alphabet = new Vector<String>();
    protected static List<Map<String, String>> transitions = new ArrayList<>();
    protected Vector<String> initialState = new Vector<String>();
    protected Vector<String> finalState = new Vector<String>();
    protected int iterationLine=0;

    //constructor which takes as input the name of
    //the file containing the automaton
	 public Automate(String fileName) throws Exception {
	        // Open the file
	        BufferedReader br = new BufferedReader(new FileReader(fileName));

		    //Variable allowing to affect each part of the automaton
	        String line;
	        String[] transition_strs;
	        String begin_state;
	        String symbol;
	        String end_state;
	        String[] Reader = null;


	        // readLine allows you to read the lines contained in the file
	        //When line is not equal to null we stay in the while loop
	        while((line = br.readLine()) != null){

	        	//In each line we check the beginning of the sentence
	            //then we remove the spaces, the useless elements and we
	            //separate the useful elements by commas
	        	if(line.startsWith("states")) {
	        		Reader= line.strip().split(":")[1].split(",");
	        		for (int i=0; i<Reader.length;i++) {
	        			this.state.add(Reader[i]);
	        		}
	        	}
	        	else if(line.startsWith("alphabet")){
	        		Reader= line.strip().split(":")[1].split(",");
	        		for (int i=0; i<Reader.length;i++) {
	        			this.alphabet.add(Reader[i]);
	        		}
	        	}
	        	else if(line.startsWith("Eo")){
	        		Reader= line.strip().split(":")[1].split(",");
	        		for (int i=0; i<Reader.length;i++) {
	        			this.initialState.add(Reader[i]);
	        		}
	        	}
	        	else if(line.startsWith("F")){
	        		Reader = line.strip().split(":")[1].split(",");
	        		for (int i=0; i<Reader.length;i++) {
	        			this.finalState.add(Reader[i]);
	        		}
	        	}
	            //When it encounters the transition sentence: in a line,
	            //it abandons this line and continues reading with
	            //the following lines.
	            //For each line following this sentence, it
	            //removes spaces and separates
	            //the elements of this line by commas.
	            //Each line must contain three elements,
	            //it assigns the first in begin_state
	            //the second in symbol and the last in end_state
	            //then adds the whole in the array transitions
	        	else if(line.startsWith("transition:")) {
	        		continue;
	        	}

	        	transition_strs=line.strip().split(",");

	        	if((transition_strs.length)>=3 && ((line.startsWith("0")
	        			|| line.startsWith("1")
                        || line.startsWith("2") || line.startsWith("3")
                        || line.startsWith("4")
                        || line.startsWith("5")))) {
        			begin_state= transition_strs[0];
        			symbol= transition_strs[1];
        			end_state= transition_strs[2];

        			Map<String, String> transition = new HashMap<>();
	        		for (int i=0; i<transition_strs.length;i++) {
	        			transition.put("begin",begin_state);
			        	transition.put("symbol", symbol);
			        	transition.put("end", end_state);
	        		}
	        		this.transitions.add(transition);
	        		iterationLine++;
	        	}
	        }

	        // Close the file
	        br.close();
	    }

	//This method allows us to recover all the alphabets.
	 //It returns each alphabet that is in the automaton entered as an argument
	public static Vector<String> getAlphabet(Automate automate) {
		 Vector<String> element_of_alphabet = new Vector<String>();
		for(String element: automate.alphabet) {
     		element_of_alphabet.add(element);
		}
		return element_of_alphabet;
	}

	//This method allows us to retrieve all the states.
	//It returns each state that is in the automaton entered as an argument
	public static Vector<String> getState(Automate automate) {
		 Vector<String> element_of_state = new Vector<String>();
		for(String element: automate.state) {
     		element_of_state.add(element);
		}
		return element_of_state;
	}

	public static Vector<String> getInitialState(Automate automate) {
		 Vector<String> element_of_initia_state = new Vector<String>();
		for(String element: automate.initialState) {
			element_of_initia_state.add(element);
		}
		return element_of_initia_state;
	}

	//This method allows us to recover the initial state or initial states.
	//It returns initial state indicated by the automaton enter as argument
	public static Vector<String> getFinalState(Automate automate) {
		 Vector<String> element_of_final_state = new Vector<String>();
		for(String element: automate.finalState) {
			element_of_final_state.add(element);
		}
		return element_of_final_state;
	}

	//This method allows us to recover all possible transitions.
	//It returns in a two-dimensional array the starting state, the symbol read and
	//the associated state or the final state of the automaton enter as argument
	public static Object[][] getTransition(Automate automate) {
		Object [][] dataRead = new Object[automate.iterationLine][automate.iterationLine];
		int i = 0;
		for(Map<String, String> map : automate.transitions) {
		    dataRead[i][0] = map.get("begin");
		    dataRead[i][1] = map.get("symbol");
		    dataRead[i][2] = map.get("end");
		    i++;
		}
		return dataRead;
	}

	//This method allows us to count all the states that
	//read the same alphabet several times.
	//In transitions, when the start state is equal
	//to the return state as argument and
	//also the alphabet read in the transition is equal
	// to the return alphabet as argument,
	//the counter variable is incremented.
    public static int commonTransitions(String state, String symbol, Automate automate) {
    	int compteur=0;
        for(Map<String, String> element: automate.transitions) {
            if(element.get("begin").equals(state) && element.get("symbol").equals(symbol)) {
            	compteur++;
            }
        }
        return compteur;
    }

    //This method allows us to draw our transition table, we create a
	//new instance of JTable using the data contained in the data variable
	//and the column names contained in the columnNames variable.
    //We also create a new instance of JFrame,
    //add the table to a JScrollPane, then add the JScrollPane to the window.
    //Finally, we define the size of the window and make it visible.
	public static void createTable(Automate automate) {
		String[] columnNames =  {"Depart", "Symbol", "Etat lié"};
		Object[][] data = Automate.getTransition(automate);

		JTable table = new JTable(data, columnNames);
		JFrame frame = new JFrame();
		frame.add(new JScrollPane(table));
		frame.setSize(300, 150);
		frame.setVisible(true);
	 }

	//this method allows us to display the initial and
	//final state or the initial and final states.
	//Just replace square brackets with braces
    public static void printStates(Automate automate) {
        System.out.println("Eo= "+
        (Automate.getInitialState(automate)).toString().replace("[", "{").replace("]", "}"));

        System.out.println("F= "+
        (Automate.getFinalState(automate)).toString().replace("[", "{").replace("]", "}"));

    }

    //this method allows us to determine if an automaton is deterministic or not.
    //It goes through each state and each symbol and passes
    //them as an argument to the CommonTransitions
    //method to retrieve the value of the counter
    //variable that will be returned by the CommonTransitions method.
    //If the counter is greater than 1 it returns False if not true.
    public static boolean is_deterministic(Automate automate) {
    	   //We go through all the states of the automaton
    	boolean resultat= true;
           for(String state: Automate.getState(automate)) {
           //We check if each state has at most one transition for each symbol
               for(String symbol: Automate.getAlphabet(automate)){
                   if ((Automate.commonTransitions(state, symbol, automate)) > 1){
                       resultat= false;
                   }
               }
           }
       //If no state has more than one transition for a symbol,
       //then the automaton is deterministic
           return resultat;
    }

    //Method that allows you to ask the user what he wants
    //after entering his file containing the automaton
    public static void operationChoice(Automate automate) {
        if(Automate.is_deterministic(automate)){
            System.out.println("You have entered a Deterministic"
            		+ " Automaton");
        }
        else {
            System.out.println("You have entered a Non Deterministic Automaton\n"
            		+ "Press 1 to convert to a Deterministic Automaton\n"
            		+ "Press 2 to cancel this operation");

            //this block manages exceptions, when
            //user enters a value than a number, it
            //will generate an error message
            try {
	            Scanner clavier= new Scanner(System.in);

	    		int operation= 0;

	    		operation= clavier.nextInt();
	    		if(operation==1) {
	    			System.out.println("This operation is currently unavailable");
	    		}
	    		while(operation==2) {
	    			break;
	    		}
            }
            catch (InputMismatchException e){
            	System.out.println("Invalid input. Please Please enter a number");
            }
        }
    }

    public static void wordSearch(Automate automate, String word) {
    	//Initialization of all the states (current state and initial state)
        //of the automaton
    	for(String state: automate.initialState) {
    		Set<String> currentStates = new HashSet<>();
    		currentStates.add(state);

    		Set<String> elementInWorld = new HashSet<>();
    		//For each symbol of the word
    		for (int i = 0; i < word.length(); i++) {
    		    char temper = word.charAt(i);
    		    String symbole= Character.toString(temper);
                //Initialization of the variable that will contain the next state
                Set<String> nextStates = new HashSet<>();

                //For each current state
                for(String currentState: currentStates) {
                	// For each transition starting from this current state
                	for(Map<String, String> transition : automate.transitions) {
                        //If the input vertex is in the current state and the symbol
                        //read in this transition is equal to the first symbol
                        //contained in the word that the user typed
                		if(transition.get("begin").equals(currentState) &&
                				transition.get("symbol").equals(symbole)) {
                			// Addition of the arrival state of this transition
                			//to the set of following states3
                			nextStates.add(transition.get("end"));
                			//Displays this symbol is in this transition
                			System.out.println(symbole+" is in the transition ("+
                					transition.get("begin")+","
                                    +transition.get("end")+")");
                		}
                		//If not add the symbol in the symbol in the table of words
                		else if(transition.get("begin") != currentState &&
                					transition.get("symbol") == symbole) {
                				elementInWorld.add(symbole);
                			}
                    //Update of all current states
                    currentStates = nextStates;
                    }
                }
            }
            //Checking if one of the current states is final
            for(String states: currentStates) {
                if(automate.finalState.contains(states)) {
                    System.out.println(word+" is recognized by the automaton");
                }
                else {
                	System.out.println(word+" is not recognized by the automaton");
                }
            }
            //If none of the current states is final,
            //the word is not recognized by the automaton
            for(String elements: elementInWorld) {
                System.out.println(elements+" is not in any transition\n");
                System.out.println(word+" is not recognized by the automaton");
            }
    }

    }

    public static void welcom() {

    	//The set of operations to choose when opening the application
    	System.out.println("Enter 1 to verify the determination of your Automaton\r\n"
        		+ "Enter 2 to convert your automaton\r\n"
        		+ "Enter 3 to test your Automaton with words\r\n"
        		+ "Enter 4 to display your automaton\r\n"
        		+ "Enter 0 to exit.");
    }


    public static void main(String[] args) throws Exception {
    	System.out.println("Welcome aboard our app.\r\n"
        		+ "Please choose an operation.\r");
    	int choice=1;
       	while(choice==1) {

	    	//creation of an instance with as argument
       		//the path where our text file is.
	    	Automate test= new Automate("C:\\Users\\djeko\\OneDrive\\Documents\\Licence3\\"
	    			+ "java\\automatejava\\src\\automatejava\\"
	    			+ "automate.txt");
	    	//greeting message display
	        test.welcom();

	        try {

		        Scanner clavier= new Scanner(System.in);

		        //Variable that will retrieve the user's choice
				int operation= 0;

				operation= clavier.nextInt();
				clavier.nextLine();

				if(operation==1) {
					Automate.operationChoice(test);
				}
				else if(operation==2) {
					System.out.println("This operation is currently unavailable");
				}
				else if(operation==3) {
					//opening of the keyboard to retrieve the user's word and
					//pass the word to the Word search function
					System.out.print("Answer: ");
					String word= clavier.nextLine();
					test.wordSearch(test, word);
				}
				else if(operation==4) {
					Automate.createTable(test);
					printStates(test);
				}
				else if(operation==0) {
					break;
				}
				else {
					System.out.println("You entered a wrong number\n");
				}

				//Call the operationChoise method when
				//the user's choice is equal to 1
				//Call the createTable method and then printStates
				//when the user's choice is equal to 4
				System.out.println("1. Continue\n0. Exit");
				System.out.println("Answer: ");
				choice= clavier.nextInt();
	        }catch (InputMismatchException e) {
	        	System.out.println("Invalid input. Please enter a number \n");
	        }
       	}

   }

}
